<?php

/** 
 * @file
 *
 * a sample transformer daemon.
 * big fat warning this should be considered psuedo code only at the moment.
 *
 * @todo update to support concurrent queue runners.
 * 
 */

//ini_set out the mem and time limits.
//include('path/to/boostrap.inc');
include('../../../../includes/bootstrap.inc');
drupal_bootstrap(BOOTSTRAP_DATABASE);
module_enable('transformer');



// @todo: command line args for priority and sleep.
$priority = 9;
$sleep = 1000;

// main loop.
while(TRUE) {
  // get next macro to process.
  $result = db_query('SELECT * FROM {transformer_queue} WHERE priority < %d LIMIT 0,1', $priority);
  $job = db_fetch_object($result);
  $macro = transformer_macro($job->macro_id);
  if (transformer_macro_perform($macro, $job->src, $job->dst, $priority)) {
    db_query("DELETE FROM {tranformer_queue} WHERE transformer_queue_id = %d", $job->transformer_queue_id);
  } 
  else {
    // watchdog job failed message.
    
    // move to end of queue on failure to prevent queue from clogging.
    $new_id = db_next_id('transformer_queue');
    db_query("UPDATE {tranformer_queue} SET transformer_queue_id = %d WHERE transformer_queue_id = %d", $new_id, $job->transformer_queue_id);
  }
  // rest for sec... we might as well give the server a few ticks to catch its breath, empty swap, play a video
  // game, etc.
  sleep($sleep);

}
